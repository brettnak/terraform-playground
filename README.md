This is a super basic terraform example that spins up:

- [x] A VPC
- [x] A Public Subnet
    - [x] An Internet Gateway
    - [x] A default route to the IGW
    - [x] A NAT Gateway
    - [x] A Bastion Host
        - [x] In a border security group exposing 22 & 80 to the internet
        - [x] Running SSH
        - [x] Running NGINX
    - [x]  An ELB for the classic app below
    - [ ]  An ELB for the ECS cluster below

- [x] A Private Subnet
    - [x] With a routing table for 0.0.0.0/0 to the NAT in the public subnet
    - [x] An application host
        - [x]  Not routable from the internet
        - [x]  Running NGINX
            - [x] Which can only be accessed through an ELB, or internally
        - [x]  Running SSH
            - [x]  Which can only be accessed by proxying through bastion
    - [ ] An ECS Cluster

# Setup

## Your computer

1. Install homebrew
2. `brew install aws`
3. `brew install terraform`
4. clone this project, cd into it.

## Configure AWS

1. Open an aws account.  I'd use your personal email address and a personal credit card.  You'll only be charged a couple dollars for everything in here, and if you just opened your account, a lot of this is in the Free tier.  You will have to put in your credit card though, and you'll probably be charged a few dollars.  (< $10)
2. Go to the IAM page and create yourself a user.
3. Generate some keys for it.
4. export those keys into your current shell:
    ```shell
    # Configure the AWS CLI
    export AWS_ACCESS_KEY_ID='your key id'
    export AWS_SECRET_ACCESS_KEY='your secret key'
    # I Chose to put everything in us-west-2, you can put them wherever you want.
    export AWS_DEFAULT_REGION='us-west-2'

    # Inject these vars into terraform.
    export TF_VAR_aws_access_key_id="$AWS_ACCESS_KEY_ID"
    export TF_VAR_aws_secret_access_key="$AWS_SECRET_ACCESS_KEY"
    export TF_VAR_aws_default_region="$AWS_DEFAULT_REGION"
    ```
    Protip: I have an encrypted volume on my computer where I put the script above.  When I want to work on stuff related to the aws account above, I `source /Volumes/Secure/terraform-demo/aws.iam.sh` to inject those variables into my shell.
5. Generate a new keypair.  You can use your own, but I wouldn't, you might as well start fresh for something like this.  I often just create some new ones like this:
    ```shell
    ssh-keygen -t rsa -b 4096 -C "some-email-address" -f /Volumes/Secure/terraform-demo/id_rsa
    # This will create
    # * /Volumes/Secure/terraform-demo/id_rsa
    # * /Volumes/Secure/terraform-demo/id_rsa.pub
    ```
    To make things even easier, append the following to your aws.iam.sh script
    ```
    TF_VAR_aws_default_public_key_path=/Volumes/Secure/terraform-demo/id_rsa.pub
    ```
6. Create a private S3 bucket with the new user
    * `aws s3api create-bucket --bucket <bucket_name> --acl private --create-bucket-configuration LocationConstraint="us-west-2"`
7. `cp backend.tf.example backend.tf`
8. edit `backend.tf`, put the name of your bucket in the place for a bucket name.
9. Install terraform. `brew install terraform`
10. Now, since we're just using an ssh script provisioner, we need to setup ssh-agent
    a. This is also a bit different because in a real production environment, actual provisioning of a host would probably happen via different mechanism of either pre-backed amis via something like packer, ansible, or puppet.
    b. `ssh-add /Volumes/Secure/terraform-demo/id_rsa`
11. `terraform get` - Downloads the modules you'll be using.
12. `terraform apply` - Creates the infrasturcture in AWS.

That's all there is to getting started with this project.  You can now click around your aws console and see the things that were made and how the terraform resource correspond to what was actually created.

## See what you did

Run `terraform output`, this gives you some information about what you created such as the ip address of the bastion host, the ip address of the classic app server, and the dns name of the elb.

Try loading the default nginx page at `http://<classic_web_dns>:80`

You _may_ need to wait up to 2 minutes for the DNS name to propagate throudh various DNS caches.

# Notes

You probably are going to want to `terraform destroy` at the end of the day.  By doing this, you should be able to get your actually charges to something less than one dollar.

This is all configured to use `us-west-2`, Oregon.  I thought about making it work for other regions, but I probably won't.

# Understanding

## Networking

Look at the following files:

* `vpc_config.tf`
* `private_subnet.tf`
* `public_subnet.tf`

## SSH access to classic-style hosts

To get into the hosts that are in the private subnet you'll need to use ssh's ProxyCommand.

```shell
ssh -o ProxyCommand='ssh -W <final_destination_ip>:%p ubuntu@<bastion_ip>' ubuntu@<final_destination_ip>
```

You can look in the statefile, or click around in the console to discvoer those things.  Or, you can scroll way up in the output of `terraform apply`, they're all there.

In an actual production-style environment you'd probably either:

1. Setup a VPN that just put your computer in the private subnet, or
2. Setup your local ssh config such that it was handled for you.

## TODO

The following items will be added as they're found useful.

* Private DNS
* Public DNS
* ECS in the private zone
    - ELB in the public zone to access it.
* Probably a kubernetes example as well.
* Something to manage IAM Roles.
* Some module examples.

# Help

I'd love to have people add some more inline comments about what the things here are.  I added some comments such that things minimally described, but if you find yourself confused, and then understand, it would be helpful to add some hints to the files to help the next person.

# Notes

This probably isn't how you'd actually want to setup a _real_ terraform repository.  This is for demonstration purposes and to make it plainly obvious in what network partition specfic resources live, hence, this repository is organized in large part by the networking layer.
