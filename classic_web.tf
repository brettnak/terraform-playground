resource "aws_elb" "web" {
  name = "elb-for-classic-app"

  subnets         = ["${aws_subnet.public.id}"]
  security_groups = ["${aws_security_group.elb.id}", "${aws_security_group.private_instances.id}"]

  instances       = ["${aws_instance.classic_app.id}"]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    interval            = 10
    target = "HTTP:80/index.nginx-debian.html"
  }

  depends_on = [ "aws_instance.classic_app" ]
}

resource "aws_instance" "classic_app" {
  depends_on = [ "aws_instance.bastion", "aws_nat_gateway.default" ]

  # Lookup the correct AMI based on the region we specified
  ami = "${lookup(var.aws_amis, var.aws_default_region)}"

  instance_type   = "t2.micro"
  vpc_security_group_ids = ["${aws_security_group.private_instances.id}"]

  subnet_id = "${aws_subnet.private.id}"

  # The connection block tells our provisioner how to
  # communicate with the resource (instance)
  connection {
    # The default username for our AMI
    user = "ubuntu"
    agent = true
    bastion_host = "${aws_instance.bastion.public_ip}"
  }

  key_name = "${aws_key_pair.auth.id}"

  # We run a remote provisioner on the instance after creating it.
  # In this case, we just install nginx and start it. By default,
  # this should be on port 80
  provisioner "remote-exec" {
    script = "./provision_scripts/install_start_nginx.sh"
  }

  tags {
    Name = "classic_app_server"
  }
}
