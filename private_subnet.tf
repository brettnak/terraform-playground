# Create a subnet to launch our instances into
resource "aws_subnet" "private" {
  vpc_id                  = "${aws_vpc.default.id}"
  cidr_block              = "10.0.2.0/24"
  availability_zone       = "${var.aws_availability_zone}"
  tags = {
    Name = "private subnet"
  }
}

# In the private subnet, we need to setup a default route that points to the
# NAT which resides in the public subnet.
resource "aws_route_table" "private" {
  vpc_id = "${aws_vpc.default.id}"

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = "${aws_nat_gateway.default.id}"
  }

  tags {
    Name = "route to NAT"
  }
}

# Associate the NAT routing table with the private subnet.
resource "aws_route_table_association" "association" {
  subnet_id = "${aws_subnet.private.id}"
  route_table_id = "${aws_route_table.private.id}"
}

# Things that are in the private subnet.
resource "aws_security_group" "private_instances" {
  name        = "private_instances"
  description = "Access between stuff in the private subnet"
  vpc_id      = "${aws_vpc.default.id}"

  # Full TCP access between instances in the private subnet.
  ingress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/16"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
