# USE TF_VAR_aws_access_key_id
variable "aws_access_key_id" {}
variable "aws_secret_access_key" {}
variable "aws_default_region" {}

variable "aws_default_keyname" {
  default = "aws_brettnak"
}

variable "aws_default_public_key_path" {
  default = "/Volumes/Secure/terraform-demo/id_rsa.pub"
}

variable "aws_availability_zone" {
  default = "us-west-2c"
}

variable "aws_amis" {
  type = "map"
  default = {
    # Ubuntu 17.10
    "us-west-2" = "ami-70873908"
  }
}
