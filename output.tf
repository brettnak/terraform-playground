output "classic_app_ip" {
  value = "${aws_instance.classic_app.private_ip}"
}

output "bastion_ip" {
  value = "${aws_instance.bastion.public_ip}"
}

output "classic_web_dns" {
  value = "${aws_elb.web.dns_name}"
}

output "classic_web_ssh" {
  value="ssh -oProxyCommand='ssh -W ${aws_instance.classic_app.private_ip}:%p ubuntu@${aws_instance.bastion.public_ip}' ubuntu@${aws_instance.classic_app.private_ip}"
}

output "classic_web_http" {
  value="curl http://${aws_elb.web.dns_name}/"
}
