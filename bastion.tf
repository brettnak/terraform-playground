resource "aws_instance" "bastion" {
  # Lookup the correct AMI based on the region we specified
  ami = "${lookup(var.aws_amis, var.aws_default_region)}"

  instance_type   = "t2.micro"
  vpc_security_group_ids = ["${aws_security_group.border_instances.id}"]

  subnet_id = "${aws_subnet.public.id}"

  # The connection block tells our provisioner how to
  # communicate with the resource (instance)
  connection {
    # The default username for our AMI
    user = "ubuntu"

    # The connection will use the local SSH agent for authentication.
  }

  key_name = "${aws_key_pair.auth.id}"

  # We run a remote provisioner on the instance after creating it.
  # In this case, we just install nginx and start it. By default,
  # this should be on port 80
  provisioner "remote-exec" {
    script = "./provision_scripts/install_start_nginx.sh"
  }

  tags {
    Name = "bastion"
  }
}
