resource "aws_vpc" "default" {
  cidr_block = "10.0.0.0/16"

  tags {
    Name = "terraform-demo-vpc"
  }
}

# This is the EC2 keypair we're going to use for all ec2 instances in the
# VPC
resource "aws_key_pair" "auth" {
  key_name   = "${var.aws_default_keyname}"
  public_key = "${file(var.aws_default_public_key_path)}"
}

resource "aws_internet_gateway" "default" {
  vpc_id = "${aws_vpc.default.id}"
}

# Grant the VPC internet access on its main route table
resource "aws_route" "internet_access" {
  route_table_id         = "${aws_vpc.default.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.default.id}"
}
