#!/bin/bash
set -e

echo "Preparing to update..."
sleep 20

sudo apt-get -y update
echo "Installing NGINX..."
sleep 5
sudo apt-get -y install nginx
echo "Starting NGINX"
sleep 5
sudo service nginx start
