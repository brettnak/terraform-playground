resource "aws_eip" "nat" {
  vpc = true
}

# This is the NAT that the private subnet will use.  Note that the NAT is
# actually located in the public subnet.  We setup a route (networking/private.tf)
# and attach it to the private subnet's routing table which defaults all
# traffic here.
resource "aws_nat_gateway" "default" {
  allocation_id = "${aws_eip.nat.id}"
  subnet_id     = "${aws_subnet.public.id}"

  tags {
    Name = "Egress NAT"
  }

  depends_on = ["aws_internet_gateway.default"]
}

# Create a subnet to launch our instances into
resource "aws_subnet" "public" {
  vpc_id                  = "${aws_vpc.default.id}"
  cidr_block              = "10.0.1.0/24"
  availability_zone       = "${var.aws_availability_zone}"

  # This is what makes this a "public" availability zone
  map_public_ip_on_launch = true
  tags = {
    Name = "public subnet"
  }
}

# A security group for the ELB so it is accessible via the web
resource "aws_security_group" "elb" {
  name        = "public_elb_sg"
  description = "Use for public-facing load balancers"
  vpc_id      = "${aws_vpc.default.id}"

  # HTTP access from anywhere
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Things that are in the public subnet AND accept connections from anywhere.
resource "aws_security_group" "border_instances" {
  name        = "border_instances"
  description = "Use for SSH access to internal stuff"
  vpc_id      = "${aws_vpc.default.id}"

  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTP access from the VPC
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
